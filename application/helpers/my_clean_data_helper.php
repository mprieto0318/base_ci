<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * ----- MODO DE USO -------

    $inputs = $this->input->post();

	$values = array(
	  'nombre_u' => $inputs['name'], 
	  'img_u' => $result_img,
	  ...
	);

    $this->load->helper('my_clean_data');
    $table = 'usuarios';
    $result = $this->Mregistro->create($table, $this->limpiar_datos($values));

    if ($result > 0) {
	  // SUCCESS
    }else {
      // ERROR
    }
 */


if (!function_exists('limpiar_datos')) {
  function limpiar_datos($values) {
    $CI =& get_instance();
    $clean = array();
    foreach ($values as $key => $value) {
      $clean[$key] = $CI->security->xss_clean(strip_tags($value));
    }
    return $clean;
  }
}