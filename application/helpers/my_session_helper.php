<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * --- MODO DE USO ---

 	public function __construct() {
	    parent::__construct();
	    // - si esta en el controlador de login
	    if(validate_session() && $this->uri->segment(2) != 'logout') {
	      redirect(base_url() . 'login');
	    }

	    // si esta en otro controlador
	    if(!validate_session()) {
	      redirect(base_url());
	    }
	}
 */

if (!function_exists('validate_session')) {
  function validate_session() {
    $CI =& get_instance();
    if ($CI->session->userdata('id_u')) {
      return TRUE;
    }else {
      return FALSE;
    }
  }
}