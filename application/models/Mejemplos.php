<?
defined('BASEPATH') OR exit('No direct script access allowed');

class Mejemplos extends CI_Model {

  public function __construct() {
    parent::__construct();
  }

   /*
   * - - - - - - - - - - - - - - - - - -- 
   *              ESCRIBIR QUERY
   * - - - - - - - - - - -- - - - - -- --
   */

  function query_write() {
    // - si se consulta retorna datos
    return $this->db->query('SELECT * FROM pruebas');

    // - db->escape : para filtrar sql injection
    return $this->db->query('SELECT * FROM pruebas WHERE name = ' . $this->db->escape($name));
    
    // - si se escribe retorna true o false
    return $this->db->query('INSERT INTO pruebas(name)VALUES("miguel");');
  }



  /*
   * - - - - - - - - - - - - - - - - - -- 
   *              INSERTAR
   * - - - - - - - - - - -- - - - - -- --
   */

  // Insertar registro
  function insert($values) {
    $this->db->insert('pruebas', $values);
    return $this->db->insert_id();
  }

   /*
   * - - - - - - - - - - - - - - - - - -- 
   *              ACTUALIZAR
   * - - - - - - - - - - -- - - - - -- --
   */
   function update($id, $values) {
     $this->db->where('id', $id);
     return $this->db->update('pruebas', $values);
   }


  /*
   * - - - - - - - - - - - - - - - - - -- 
   *              ELIMINAR
   * - - - - - - - - - - -- - - - - -- --
   */
  function delete($id) {
    $this->db->where('id', $id);
    return $this->db->delete('pruebas');
  }



  
  /*
   * - - - - - - - - - - - - - - - - - -- 
   *              SELECCIONAR
   * - - - - - - - - - - -- - - - - -- --
   */

  // Seleccionar todo y retornar objeto
  function select_all_objeto() {
    return $this->db->get('pruebas')->result();
    // - Limit
    //$this->db->get('pruebas', 10, 20)->result();
  }

  // Seleccionar todo y retornar array
  function select_all_array() {
    return $this->db->get('pruebas')->result_array();
    // - Limit
    //$this->db->get('pruebas', 10, 20)->result_array();
  }

  // Seleccionar todo donde where
  function select_all_where($id) {
    $this->db->from('pruebas');
    $this->db->where('id', $id);
    return $this->db->get()->row();

    // $this->db->where('name !=', $name);
    // $this->db->where('id <', $id); // Produces: WHERE name != 'Joe' AND id < 45
    // ---
    //$array = array('name !=' => $name, 'id <' => $id, 'date >' => $date);
    //$this->db->where($array);
    // ---
    // $where = "name='Joe' AND status='boss' OR status='active'";
    // $this->db->where($where);
    // --- OR WHERE
    // $this->db->where('name !=', $name);
    // $this->db->or_where('id >', $id);  // Produces: WHERE name != 'Joe' OR id > 50
    // --- IN WHERE
    // $names = array('Frank', 'Todd', 'James');
    // $this->db->where_in('username', $names);
    // // Produces: WHERE username IN ('Frank', 'Todd', 'James')
    // --- OR IN WHERE
    // $names = array('Frank', 'Todd', 'James');
    // $this->db->or_where_in('username', $names);
    // // Produces: OR username IN ('Frank', 'Todd', 'James')
    // --- NOT IN WHERE
    // $names = array('Frank', 'Todd', 'James');
    // $this->db->where_not_in('username', $names);
    // --- NOT OR IN WHERE
    // $names = array('Frank', 'Todd', 'James');
    // $this->db->or_where_not_in('username', $names);
    // // Produces: OR username NOT IN ('Frank', 'Todd', 'James')        

  }


  // Seleccionar todo 
  // Join, Orderby
  function select_all_propiedades_1() {
    $this->db->select('p.id, p.name, p.last_name, p.age, p.birthdate, c.name city');
    $this->db->from('pruebas p');
    $this->db->join('ciudad c', 'c.id = p.city');
    $this->db->order_by('p.id', 'desc');
    return $this->db->get()->result();
  }


  function select_limit($inicio = FALSE, $limite = FALSE) {

    $this->db->select('p.id, p.name, p.last_name, p.age, p.birthdate, c.name city');
    $this->db->from('pruebas p');
    $this->db->join('ciudad c', 'c.id = p.city');
    $this->db->order_by('p.id', 'desc');
    $this->db->limit($limite, $inicio);
    return $this->db->get()->result();

  }


  /*
   *  ------------------------------------------------------------
   *  - - - - - - - - - - - -  OTROS - - - - - - -  -- - -  - - -
   * https://codeigniter.com/userguide3/database/query_builder.html
   * *  ------------------------------------------------------------
   */



}