<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chome extends CI_Controller {

	// Home Frontend: /application/views/pages/frontend/Vhome.php
	public function index(){
		// Configutacion de la pagina
    $data['title_page'] = 'Pagina del Frontend';
    $data['rol_render'] = 'frontend';
    $data['page_render'] = 'Vhome';
    $data['show_footer'] = TRUE;
    $data['show_sidebar'] = TRUE;
    $data['menu_active'] = 'Vhome';

    $this->load->view('template', $data);
  }

	// Home Backend: /application/views/pages/backend/Vhome.php
  function home_back() {
		// Configutacion de la pagina
    $data['title_page'] = 'Pagina del BackEnd';
    $data['rol_render'] = 'backend';
    $data['page_render'] = 'Vhome';
    $data['show_footer'] = TRUE;
    $data['show_sidebar'] = TRUE;
    $data['menu_active'] = 'Vhome';
    $this->load->view('template', $data);
  }


  /* 
   * ----------------------------------------------------------------
   * - - - - - - - - - - - - - - - - -
   *   Ejemplos de consultas a la db
   * - - - - - - - - - - - - - - - - -
   * ----------------------------------------------------------------
   */


  /*
   * --- Mejor uso crear un helper con esta funcion ---
   * Limpiar datos inputs de xss y strip_tags
   */
  function limpiar_datos($values) {
    $clean = array();
    foreach ($values as $key => $value) {
      $clean[$key] = $this->security->xss_clean(strip_tags($value));
    }
    return $clean;
  }

  /*
   * Insertar Registro
   */
  function db_insert() {
    $this->load->model('Mejemplos');

    $values = array(
      'name' => 'Miguel ' . mt_rand(0, 100),
      'last_name' => 'prieto ' . mt_rand(0, 100), 
      'age' => mt_rand(10, 70),
      'birthdate' => date('Y-m-d'),
      'active' => 1,
      'city' => mt_rand(1, 8),
    );

    $result = $this->Mejemplos->insert($this->limpiar_datos($values));

    if ($result > 0) {
      echo 'Registro creado con el id: ' . $result;
    }else {
      echo 'Error al crear el registro. <br>';
    }
  }

  /*
   * Actualizar Registro
   */
  function db_update() {
    $this->load->model('Mejemplos');

    $id = 18;

    $values = array(
      'name' => 'nomber actualizado',
      'last_name' => 'apellido actualizado ', 
      'age' => mt_rand(10, 70),
      'birthdate' => date('Y-m-d'),
      'active' => 0,
      'city' => mt_rand(1, 8),
    );

    $result = $this->Mejemplos->update($id, $this->limpiar_datos($values));
    if ($result) {  // $result =  TRUE or FALSE
      echo 'Se actualizo el registro';
    }else {
      echo 'Error al actualizar el registro. <br>';
    }
  }


  /*
   * Eliminar Registro
   */
  function db_delete() {
    $this->load->model('Mejemplos');

    $id = 20;

    $result = $this->Mejemplos->delete($id);

    if ($result) {  // $result =  TRUE or FALSE
      echo 'Se elimino el registro';
    }else {
      echo 'Error al elimino el registro. <br>';
    }
  }



  /*
   * Seleccionar todo (Select *)
   * Datos retornados de tipo objeto
   */
  function db_select_all_objeto() {
    $this->load->model('Mejemplos');
    $result = $this->Mejemplos->select_all_objeto();

    foreach ($result as $value) {
      echo 'ID: ' . $value->id . '<br>';
      echo $value->name . ' ' . $value->last_name . '<br>';
      echo $value->age . ' ' . $value->birthdate . '<br>';
      echo $value->city . '<br>';
      echo '<hr>';
    }
  }


  /*
   * Seleccionar todo (Select *)
   * Datos retornados de tipo array
   */
  function db_select_all_array() {
    $this->load->model('Mejemplos');
    $result = $this->Mejemplos->select_all_array();

    foreach ($result as $value) {
      echo 'ID: ' . $value['id'] . '<br>';
      echo $value['name'] . ' ' . $value['last_name'] . '<br>';
      echo $value['age'] . ' ' . $value['birthdate'] . '<br>';
      echo $value['city'] . '<br>';
      echo '<hr>';
    }
  }


  /*
   * Seleccionar todo donde(Select * Where)
   */
  function db_select_all_where() {
    $this->load->model('Mejemplos');

    $id = 1;
    $result = $this->Mejemplos->select_all_where($id);

    if (!empty($result)) {
      echo 'ID: ' . $result->id . '<br>';
      echo $result->name . ' ' . $result->last_name . '<br>';
      echo $result->age . ' ' . $result->birthdate . '<br>';
      echo $result->city . '<br>';
      echo '<hr>';
    }else {
      echo 'No se encontro ningun resultado';
    }
    
  }

  function select_limit($inicio = 0, $limite = 10) {
    $this->load->model('Mejemplos');
    $result = $this->Mejemplos->select_limit($inicio, $limite);

    $output = '';
    if (!empty($result)) {
      
      $next = $inicio + $limite;

      $output .= '<b> ------> PAGINA ' . $next . '</b>';

      foreach ($result as $value) {
        $output .= '<h3>ID: ' . $value->id . '</h3>';
        $output .= '<p>'. $value->name . ' ' . $value->last_name . '</p>';
        $output .= '<p>'. $value->age . ' ' . $value->birthdate . '</p>';
        $output .= '<p>'. $value->city . '</p>';
        $output .= '<hr>';
      }

      $output .= '<a href="' . base_url() . 'chome/select_limit/' . $next . '">next page</a>';
    }else {
       $output .= '<b> No hay mas resultados</b>';
    }
    echo $output;
  }


  /*
   * Seleccionar todo (Select *)
   * Join, Orderby
   */
  function db_select_all_propiedades_1() {
    $this->load->model('Mejemplos');
    $result = $this->Mejemplos->select_all_propiedades_1();

    foreach ($result as $value) {
      echo 'ID: ' . $value->id . '<br>';
      echo $value->name . ' ' . $value->last_name . '<br>';
      echo $value->age . ' ' . $value->birthdate . '<br>';
      echo $value->city . '<br>';
      echo '<hr>';
    }
  }

}
