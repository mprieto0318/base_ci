<?php
  $attr_dropdown = array(
    'class' => 'dropdown-toggle', 
    'data-toggle' => 'dropdown'
  );
?>

<!DOCTYPE html>
<html>
<head>
	<title><?= $title_page; ?></title>
	<link rel="stylesheet" rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/contrib/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/css/contrib/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= base_url();?>assets/css/custom/style.css">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
</head>
<body>

	<div class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        
        <?= anchor(base_url(), '<span>APP</span>', array('class' => 'navbar-brand')); ?>
      </div>
      <div class="collapse navbar-collapse" id="navbar-ex-collapse">
        <ul class="nav navbar-nav navbar-right">
          
          <li class=<?php ($menu_active == 'Vhome') ? print 'active' : ''; ?> >
            <?= anchor(base_url() . 'front/home', '<i class="fa fa-star fa-fw"></i> Home FronEnd'); ?>
          </li>

          <li>
            <?= anchor(base_url() . 'back/home', '<i class="fa fa-star fa-fw"></i> Home BackEnd'); ?>
          </li>

          <li class="dropdown">
            <?= anchor('#', 'Utilidades <span class="caret"></span>', $attr_dropdown); ?>
            <ul class="dropdown-menu">
              <li>
                <?= anchor(base_url() . 'utilidad1', 'utilidad 1'); ?>
              </li>
              <li>
                <?= anchor(base_url() . 'utilidad2', 'utilidad 2'); ?>
              </li>
              <li>
                <?= anchor(base_url() . 'utilidad3', 'utilidad 3'); ?>
              </li>
            </ul>
          </li>

        </ul>
      </div>
    </div>
  </div>

  <div class="section">
    <div class="container-fluid text-center">
      <div class="row">
        <div class="col-md-12">
          <div class="page-header">
            <h1><?= $title_page; ?></h1>
          </div>
        </div>
      </div>
    </div>
  </div>

