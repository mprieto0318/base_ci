<?php

// Header
$this->load->view('layout/' . $rol_render . '/header');
$this->load->view('global/open_section');

// Content
$this->load->view('pages/' . $rol_render . '/' . $page_render);

// Sidebar
if ($show_sidebar) {
	$this->load->view('layout/' . $rol_render . '/sidebar');
}

$this->load->view('global/close_section');

// Footer
if ($show_footer) {
	$this->load->view('layout/' . $rol_render . '/footer');
}

$this->load->view('global/scripts.php');
$this->load->view('global/close_html.php');